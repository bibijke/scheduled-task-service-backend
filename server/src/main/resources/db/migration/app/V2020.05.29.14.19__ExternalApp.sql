CREATE TABLE public.external_app
(
  id           BIGSERIAL PRIMARY KEY NOT NULL,
  name         VARCHAR(255)          NOT NULL,
  base_url     VARCHAR(255)          NOT NULL,
  access_token VARCHAR(50)           NOT NULL
);

CREATE UNIQUE INDEX external_app_token_uidx
  ON public.external_app (access_token);
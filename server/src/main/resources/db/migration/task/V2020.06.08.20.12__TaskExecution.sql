ALTER TABLE public.task_execution
    DROP COLUMN description;

ALTER TABLE public.task_execution
    ADD COLUMN description TEXT NULL DEFAULT NULL;
CREATE TABLE public.task_execution_last
(
    task_id      BIGINT NOT NULL,
    execution_id BIGINT NOT NULL,
    CONSTRAINT task__fk FOREIGN KEY (task_id) REFERENCES public.task (id),
    CONSTRAINT execution__fk FOREIGN KEY (execution_id) REFERENCES public.task_execution (id)
);

CREATE UNIQUE INDEX task_execution_last_uidx
    ON public.task_execution_last (task_id, execution_id DESC);
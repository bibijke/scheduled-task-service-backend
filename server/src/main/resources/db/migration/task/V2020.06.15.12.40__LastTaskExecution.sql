ALTER TABLE public.task_execution_last
    DROP CONSTRAINT execution__fk;

ALTER TABLE public.task_execution_last
    ADD CONSTRAINT execution__fk
        FOREIGN KEY (execution_id) references public.task_execution
            ON DELETE CASCADE;
CREATE TABLE public.task
(
    id          BIGSERIAL PRIMARY KEY NOT NULL,
    schedule    VARCHAR(20) NOT NULL,
    name        VARCHAR(255) NOT NULL,
    update_date TIMESTAMP NOT NULL DEFAULT now(),
    app_id      BIGINT NOT NULL,
    CONSTRAINT app__fk FOREIGN KEY (app_id) REFERENCES public.external_app (id)
);

CREATE INDEX task_app_idx
    ON public.task (app_id);
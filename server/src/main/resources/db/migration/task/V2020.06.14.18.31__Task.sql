ALTER TABLE public.task
    DROP COLUMN uri;

ALTER TABLE public.task
    DROP COLUMN schedule;

ALTER TABLE public.task
    ADD COLUMN schedule VARCHAR(20) NULL DEFAULT NULL;
ALTER TABLE public.task_execution
    ADD COLUMN rate REAL NOT NULL DEFAULT 0;

ALTER TABLE public.task_execution
    ADD COLUMN duration BIGINT NOT NULL DEFAULT 0;

ALTER TABLE public.task_execution
    ADD COLUMN remaining BIGINT NOT NULL DEFAULT 0;
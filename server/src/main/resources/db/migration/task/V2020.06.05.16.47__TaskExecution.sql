CREATE TABLE public.task_execution
(
    id          BIGSERIAL PRIMARY KEY NOT NULL,
    task_id     BIGINT                NOT NULL,
    state       INT                   NOT NULL,
    start_date  TIMESTAMP             NOT NULL,
    end_date    TIMESTAMP             NULL DEFAULT NULL,
    description VARCHAR(500)          NULL DEFAULT NULL,
    CONSTRAINT task__fk FOREIGN KEY (task_id) REFERENCES public.task (id)
);

CREATE INDEX execution_task_idx
    ON public.task_execution (task_id, id DESC);
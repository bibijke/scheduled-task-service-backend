package com.koronaauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScheduledTaskService {
    public static void main(String[] args) {
        SpringApplication.run(ScheduledTaskService.class, args);
    }
}
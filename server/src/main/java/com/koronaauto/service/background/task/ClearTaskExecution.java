package com.koronaauto.service.background.task;

import com.koronaauto.scheduledtaskservice.service.Client;
import com.koronaauto.scheduledtaskservice.service.impl.task.NonConcurrentTask;
import com.koronaauto.service.task.execution.TaskExecutionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClearTaskExecution extends NonConcurrentTask {
    private final Logger logger = LoggerFactory.getLogger(ClearTaskExecution.class);

    private final TaskExecutionService taskExecutionService;

    @Autowired
    public ClearTaskExecution(Client client, TaskExecutionService taskExecutionService) {
        super(TaskType.CLEAR_TASK_EXECUTION.getId(), client);

        this.taskExecutionService = taskExecutionService;
    }

    @Override
    protected void perform() {
        taskExecutionService.deleteExpired();

        setProgress(1);
    }

    @Override
    protected void onSendProgressException(Exception e) {
        logger.info("Send progress error", e);
    }
}

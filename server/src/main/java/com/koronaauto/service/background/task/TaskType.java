package com.koronaauto.service.background.task;

public enum TaskType {
    CLEAR_TASK_EXECUTION(1)
    ;
    private final long id;

    TaskType(int id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }
}

package com.koronaauto.service.impl.task.execution;

import com.koronaauto.entity.impl.task.LastTaskExecution;
import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.entity.impl.task.TaskExecution;
import com.koronaauto.repository.task.LastTaskExecutionRepository;
import com.koronaauto.service.task.execution.LastTaskExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LastTaskExecutionServiceImpl implements LastTaskExecutionService {
    private final LastTaskExecutionRepository repo;

    @Autowired
    public LastTaskExecutionServiceImpl(LastTaskExecutionRepository repo) {
        this.repo = repo;
    }

    @Override
    public TaskExecution getLastExecution(Task task) {
        return Optional
            .ofNullable(get(task))
            .map(LastTaskExecution::getExecution)
            .orElse(null);
    }

    private LastTaskExecution get(Task task) {
        return repo.findByTask(task);
    }

    @Override
    public LastTaskExecution update(Task task, TaskExecution execution) {
        LastTaskExecution stored = get(task);

        if (stored == null) {
            stored = new LastTaskExecution();

            stored.setTask(task);
        }

        stored.setExecution(execution);

        return repo.save(stored);
    }
}

package com.koronaauto.service.impl.task.job;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.service.task.job.TaskJobKeyFactory;
import org.quartz.JobKey;
import org.springframework.stereotype.Service;

@Service
public class TaskJobKeyFactoryImpl implements TaskJobKeyFactory {
    @Override
    public JobKey getStartJobKey(Task task) {
        return JobKey.jobKey(task.getId().toString(), task.getApp().getId().toString());
    }
}

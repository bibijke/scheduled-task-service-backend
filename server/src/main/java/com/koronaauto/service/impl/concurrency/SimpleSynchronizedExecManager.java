package com.koronaauto.service.impl.concurrency;

import com.koronaauto.service.concurrency.SynchronizedExecManager;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Supplier;

public class SimpleSynchronizedExecManager<T> implements SynchronizedExecManager<T> {
    private final ConcurrentMap<T, Lock> lockMap = new ConcurrentHashMap<>();

    private final Map<T, Integer> lockCountMap = new HashMap<>();

    public void execute(T key, Runnable task) {
        execute(key, () -> {
            task.run();

            return null;
        });
    }

    public <R> R execute(T key, Supplier<R> task) {
        Lock lock;

        synchronized (lockMap) {
            lock = lockMap.get(key);

            if (lock == null) {
                lock = new ReentrantLock();

                lockMap.put(key, lock);
            }

            lockCountMap.put(key, lockCountMap.getOrDefault(key, 0) + 1);
        }

        R result;

        try {
            lock.lock();

            result = task.get();
        } finally {
            lock.unlock();

            synchronized (lockMap) {
                lockCountMap.put(key, lockCountMap.getOrDefault(key, 0) - 1);

                if (lockMap.containsKey(key)) {
                    if (lockCountMap.get(key) == 0) {
                        lockMap.remove(key);

                        lockCountMap.remove(key);
                    }
                }
            }
        }

        return result;
    }
}

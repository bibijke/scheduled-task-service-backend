package com.koronaauto.service.impl.task.execution;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.entity.impl.task.TaskExecution;
import com.koronaauto.entity.impl.task.TaskExecutionState;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;
import com.koronaauto.service.app.ExternalAppClient;
import com.koronaauto.service.concurrency.SynchronizedExecManager;
import com.koronaauto.service.impl.concurrency.SimpleSynchronizedExecManager;
import com.koronaauto.service.task.TaskScheduleService;
import com.koronaauto.service.task.execution.ExternalAppTaskExecutionService;
import com.koronaauto.service.task.execution.LastTaskExecutionService;
import com.koronaauto.service.task.execution.TaskExecutionService;
import com.koronaauto.service.task.execution.TaskStartService;
import com.koronaauto.spring.qualifier.task.TaskSynchronizedExecManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
public class TaskStartServiceImpl implements TaskStartService {
    private static final Logger logger = LoggerFactory.getLogger(TaskStartServiceImpl.class);

    private final TaskExecutionService taskExecutionService;

    private final TaskScheduleService taskScheduleService;

    private final ExternalAppTaskExecutionService externalAppTaskExecutionService;

    private final ExternalAppClient externalAppClient;

    private final LastTaskExecutionService lastTaskExecutionService;

    private final SynchronizedExecManager<Long> taskSynchronizedExecManager;

    @Autowired
    public TaskStartServiceImpl(
        TaskExecutionService taskExecutionService,
        TaskScheduleService taskScheduleService,
        ExternalAppTaskExecutionService externalAppTaskExecutionService,
        ExternalAppClient externalAppClient,
        LastTaskExecutionService lastTaskExecutionService,
        @TaskSynchronizedExecManager SynchronizedExecManager<Long> taskSynchronizedExecManager
    ) {
        this.taskExecutionService = taskExecutionService;
        this.taskScheduleService = taskScheduleService;
        this.externalAppTaskExecutionService = externalAppTaskExecutionService;
        this.externalAppClient = externalAppClient;
        this.lastTaskExecutionService = lastTaskExecutionService;
        this.taskSynchronizedExecManager = taskSynchronizedExecManager;
    }

    @Override
    public void start(Task task) {
        Long taskId = task.getId();

        if (!task.isActive()) {
            throw new IllegalArgumentException("Attempt to start inactive task #" + taskId);
        }

        taskSynchronizedExecManager.execute(taskId, () -> executeSynchronized(task));
    }

    private void executeSynchronized(Task task) {
        TaskExecution execution = new TaskExecution();

        execution.setTask(task);
        execution.setStartDate(LocalDateTime.now());

        try {
            TaskExecution lastTaskExecution = externalAppTaskExecutionService.refreshProgress(task);

            boolean isFinished = Optional
                .ofNullable(lastTaskExecution)
                .map(TaskExecution::isFinished)
                .orElse(true);

            if (!isFinished) {
                handleTaskStartException(execution, "Last task execution has not been finished yet", false);

                return;
            }

            StartResponse startResponse = sendStartRequest(task);

            if (startResponse.isStarted()) {
                handleTaskStarted(execution);
            } else {
                handleTaskStartException(execution, null, false);
            }
        } catch (Exception e) {
            logger.info("Error during execution task #" + task.getId(), e);

            String errorMsg = Optional
                .ofNullable(e.getMessage())
                .orElse(e.getClass().getName());

            handleTaskStartException(execution, errorMsg, true);
        }
    }

    private StartResponse sendStartRequest(Task task) {
        return externalAppClient.start(task);
    }

    private void handleTaskStarted(TaskExecution execution) {
        execution.setState(TaskExecutionState.RUNNING);
        execution.setDescription(null);

        execution = taskExecutionService.update(execution);

        lastTaskExecutionService.update(execution.getTask(), execution);
    }

    private void handleTaskStartException(TaskExecution execution, String errorMessage, boolean restart) {
        execution.setState(TaskExecutionState.START_ERROR);

        execution.setEndDate(LocalDateTime.now());
        execution.setDescription(errorMessage);

        execution = taskExecutionService.update(execution);

        lastTaskExecutionService.update(execution.getTask(), execution);

        if (restart) {
            LocalDateTime startAt = LocalDateTime
                .now()
                .plus(3, ChronoUnit.MINUTES);

            taskScheduleService.scheduleOnce(execution.getTask(), startAt);
        }
    }
}

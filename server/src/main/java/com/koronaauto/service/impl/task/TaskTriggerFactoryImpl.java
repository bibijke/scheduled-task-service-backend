package com.koronaauto.service.impl.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.service.task.TaskTriggerFactory;
import com.koronaauto.service.task.job.TaskJobKeyFactory;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

@Service
public class TaskTriggerFactoryImpl implements TaskTriggerFactory {
    private final TaskJobKeyFactory keyFactory;

    @Autowired
    public TaskTriggerFactoryImpl(TaskJobKeyFactory keyFactory) {
        this.keyFactory = keyFactory;
    }

    @Override
    public Trigger getCronTrigger(Task task) {
        return newTrigger()
            .withIdentity(getKey(task, "cron"))
            .withSchedule(cronSchedule(task.getSchedule()))
            .forJob(keyFactory.getStartJobKey(task))
            .build();
    }

    @Override
    public Trigger getSimpleTrigger(Task task, LocalDateTime startDate) {
        Date date = Date.from(startDate.atZone(ZoneId.systemDefault()).toInstant());

        return newTrigger()
            .withIdentity(getKey(task, "once"))
            .startAt(date)
            .forJob(keyFactory.getStartJobKey(task))
            .build();
    }

    private TriggerKey getKey(Task task, String postfix) {
        return TriggerKey.triggerKey(
            task.getId().toString()+"_"+postfix,
            task.getApp().getId().toString()
        );
    }
}
package com.koronaauto.service.impl.app;

import com.koronaauto.entity.impl.app.ExternalApp;
import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.exception.app.ExternalAppClientException;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;
import com.koronaauto.service.app.ExternalAppClient;
import com.koronaauto.spring.profile.annotation.DevProfile;
import com.koronaauto.spring.profile.annotation.ProdProfile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@ProdProfile
@DevProfile
@Service
public class HttpExternalAppClient implements ExternalAppClient {
    private final RestTemplate restTemplate;

    public HttpExternalAppClient() {
        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();

        httpRequestFactory.setConnectTimeout(3000);
        httpRequestFactory.setReadTimeout(3000);

        this.restTemplate = new RestTemplate(httpRequestFactory);
    }

    private URI getActionURI(Task task, String action) {
        UriBuilder builder = UriComponentsBuilder
            .fromHttpUrl(task.getApp().getBaseUrl());

        builder.path("/" + action);

        builder.queryParam("tasks", task.getId());

        return builder.build();
    }

    private <T> HttpEntity<T> getRequest(ExternalApp app, T content) {
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set(HttpHeaders.AUTHORIZATION, app.getAccessToken());

        return new HttpEntity<>(content, headers);
    }

    @Override
    public TaskProgress getProgress(Task task) throws ExternalAppClientException {
        try {
            TaskProgress[] taskProgresses = restTemplate.exchange(
                getActionURI(task, "progress"),
                HttpMethod.GET,
                getRequest(task.getApp(),null),
                TaskProgress[].class
            ).getBody();

            List<TaskProgress> taskProgressList = Arrays.asList(Objects.requireNonNull(taskProgresses));

            return taskProgressList.get(0);
        } catch (RestClientException e) {
            throw new ExternalAppClientException(e.getMessage(), e);
        }

    }

    @Override
    public StartResponse start(Task task) throws ExternalAppClientException {
        try {
            StartResponse[] startResponses = restTemplate.exchange(
                getActionURI(task, "start"),
                HttpMethod.POST,
                getRequest(task.getApp(),null),
                StartResponse[].class
            ).getBody();

            List<StartResponse> startResponseList = Arrays.asList(Objects.requireNonNull(startResponses));

            return startResponseList.get(0);
        } catch (RestClientException e) {
            throw new ExternalAppClientException(e.getMessage(), e);
        }
    }
}

package com.koronaauto.service.impl.app;

import com.koronaauto.entity.impl.app.ExternalApp;
import com.koronaauto.repository.app.ExternalAppRepository;
import com.koronaauto.service.app.ExternalAppService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExternalAppServiceImpl implements ExternalAppService {
    private final ExternalAppRepository repo;

    @Autowired
    public ExternalAppServiceImpl(ExternalAppRepository repo) {
        this.repo = repo;
    }

    @Override
    public ExternalApp get(Long id) {
        return repo
            .findById(id)
            .orElse(null);
    }
}

package com.koronaauto.service.impl.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.repository.task.TaskRepository;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.transport.mapping.task.TaskMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Service
public class TaskServiceImpl implements TaskService {
    private final TaskRepository repo;

    private final TaskMapper taskMapper;

    @Autowired
    public TaskServiceImpl(
        TaskRepository repo,
        TaskMapper taskMapper
    ) {
        this.repo = repo;
        this.taskMapper = taskMapper;
    }

    @Override
    public Task updateOrCreate(Task source) {
        Task stored;

        if (source.getId() != null) {
            stored = repo
                .findById(source.getId())
                .orElse(null);

            if (stored == null) {
                throw new IllegalArgumentException("Task #"+source.getId()+" does not exist");
            }
        } else {
            stored = source;
        }

        taskMapper.copyToDomain(source, stored);

        stored = repo.save(stored);

        return stored;
    }

    @Override
    public Task get(Long id) {
        return repo.findById(id).orElse(null);
    }

    @Override
    public Task get(Task source) {
        return Optional
            .ofNullable(source)
            .map(Task::getId)
            .map(repo::findById)
            .flatMap(Function.identity())
            .orElse(null);
    }

    @Override
    public List<Task> getActiveWithSchedule() {
        return repo.findByActiveTrueAndScheduleNotNull();
    }

    @Override
    public List<Task> getActive() {
        return repo.findByActiveTrue();
    }
}

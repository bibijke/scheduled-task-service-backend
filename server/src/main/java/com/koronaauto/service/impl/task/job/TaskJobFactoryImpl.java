package com.koronaauto.service.impl.task.job;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.service.task.job.TaskJobFactory;
import com.koronaauto.service.task.job.TaskJobKeyFactory;
import org.quartz.JobDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static org.quartz.JobBuilder.newJob;

@Service
public class TaskJobFactoryImpl implements TaskJobFactory {
    private final TaskJobKeyFactory keyFactory;

    @Autowired
    public TaskJobFactoryImpl(TaskJobKeyFactory keyFactory) {
        this.keyFactory = keyFactory;
    }

    @Override
    public JobDetail getStartJob(Task task) {
        return newJob(TaskStartJob.class)
            .withIdentity(keyFactory.getStartJobKey(task))
            .usingJobData("task", task.getId())
            .storeDurably()
            .build();
    }
}

package com.koronaauto.service.impl.task.execution;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.entity.impl.task.TaskExecution;
import com.koronaauto.entity.impl.task.TaskExecutionState;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskState;
import com.koronaauto.service.app.ExternalAppClient;
import com.koronaauto.service.concurrency.SynchronizedExecManager;
import com.koronaauto.service.task.execution.ExternalAppTaskExecutionService;
import com.koronaauto.service.task.execution.LastTaskExecutionService;
import com.koronaauto.service.task.execution.TaskExecutionService;
import com.koronaauto.spring.qualifier.task.TaskSynchronizedExecManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExternalAppTaskExecutionServiceImpl implements ExternalAppTaskExecutionService {
    private final LastTaskExecutionService lastTaskExecutionService;

    private final ExternalAppClient externalAppClient;

    private final TaskExecutionService taskExecutionService;

    private final SynchronizedExecManager<Long> taskSynchronizedExecManager;

    @Autowired
    public ExternalAppTaskExecutionServiceImpl(
        LastTaskExecutionService lastTaskExecutionService,
        ExternalAppClient externalAppClient,
        TaskExecutionService taskExecutionService,
        @TaskSynchronizedExecManager SynchronizedExecManager<Long> taskSynchronizedExecManager
    ) {
        this.lastTaskExecutionService = lastTaskExecutionService;
        this.externalAppClient = externalAppClient;
        this.taskExecutionService = taskExecutionService;
        this.taskSynchronizedExecManager = taskSynchronizedExecManager;
    }

    @Override
    public TaskExecution refreshProgress(Task task) {
        final TaskExecution execution = lastTaskExecutionService.getLastExecution(task);

        return taskSynchronizedExecManager.execute(
            task.getId(),
            () -> refreshProgressSynchronized(execution, getProgress(task))
        );
    }

    @Override
    public void refreshProgress(Task task, TaskProgress progress) {
        TaskExecution execution = lastTaskExecutionService.getLastExecution(task);

        taskSynchronizedExecManager.execute(
            task.getId(),
            () -> refreshProgressSynchronized(execution, progress)
        );
    }

    private TaskExecution refreshProgressSynchronized(TaskExecution execution, TaskProgress progress) {
        if (execution == null) {
            return null;
        }

        if (!execution.isFinished()) {
            TaskState remoteTaskState = progress.getState();

            if (remoteTaskState.equals(TaskState.COMPLETED)) {
                execution.setState(TaskExecutionState.COMPLETED);
            }

            if (remoteTaskState.equals(TaskState.FAILED)) {
                execution.setState(TaskExecutionState.FAILED);
                execution.setDescription(progress.getErrorMessage());
            }

            execution.setEndDate(progress.getEndDate());
            execution.setRate(progress.getRate());
            execution.setDuration(progress.getDurationMs());
            execution.setRemaining(progress.getRemainingTimeMs());

            execution = taskExecutionService.update(execution);
        }

        return execution;
    }

    private TaskProgress getProgress(Task task) {
        return externalAppClient.getProgress(task);
    }
}

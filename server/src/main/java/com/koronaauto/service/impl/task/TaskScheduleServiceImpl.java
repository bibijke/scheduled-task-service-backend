package com.koronaauto.service.impl.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.exception.task.TaskScheduleException;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.service.task.TaskTriggerFactory;
import com.koronaauto.service.task.job.TaskJobFactory;
import com.koronaauto.service.task.TaskScheduleService;
import com.koronaauto.service.task.job.TaskJobKeyFactory;
import com.koronaauto.spring.profile.annotation.DevProfile;
import com.koronaauto.spring.profile.annotation.ProdProfile;
import org.quartz.*;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;

@ProdProfile
@DevProfile
@Service
public class TaskScheduleServiceImpl implements TaskScheduleService {
    private final TaskService taskService;

    private final Scheduler scheduler;

    private final TaskJobFactory taskJobFactory;

    private final TaskTriggerFactory taskTriggerFactory;

    private final TaskJobKeyFactory taskJobKeyFactory;

    public TaskScheduleServiceImpl(
        TaskService taskService,
        Scheduler scheduler,
        TaskJobFactory taskJobFactory,
        TaskTriggerFactory taskTriggerFactory,
        TaskJobKeyFactory taskJobKeyFactory
    ) {
        this.taskService = taskService;
        this.scheduler = scheduler;
        this.taskJobFactory = taskJobFactory;
        this.taskTriggerFactory = taskTriggerFactory;
        this.taskJobKeyFactory = taskJobKeyFactory;
    }

    @Override
    public void scheduleAll() throws TaskScheduleException {
        Collection<Task> tasks = taskService.getActiveWithSchedule();

        for (Task t : tasks) {
            schedule(t);
        }
    }

    @Override
    public void schedule(Task task) throws TaskScheduleException {
        Trigger trigger = taskTriggerFactory.getCronTrigger(task);

        if (task.getSchedule() == null) {
            throw new TaskScheduleException("Unable to schedule task with schedule = null");
        }

        schedule(task, trigger);
    }

    @Override
    public void scheduleOnce(Task task, LocalDateTime startAt) {
        Trigger trigger = taskTriggerFactory.getSimpleTrigger(task, startAt);

        schedule(task, trigger);
    }

    private void schedule(Task task, Trigger trigger) {
        if (!task.isActive()) {
            throw new TaskScheduleException("Unable to schedule inactive task");
        }

        TriggerKey key = trigger.getKey();

        boolean isReschedule;

        try {
            isReschedule = scheduler.getTrigger(key) != null;
        } catch (SchedulerException e) {
            throw new TaskScheduleException("Unable to schedule task #" + task.getId(), e);
        }

        JobKey jobKey = taskJobKeyFactory.getStartJobKey(task);

        // Add durable job if not exists
        try {
            if (!scheduler.checkExists(jobKey)) {
                JobDetail job = taskJobFactory.getStartJob(task);

                scheduler.addJob(job, false);
            }
        } catch (SchedulerException e) {
            throw new TaskScheduleException("Unable to schedule task #" + task.getId(), e);
        }

        // Schedule job with new trigger
        try {
            if (isReschedule) {
                scheduler.rescheduleJob(key, trigger);
            } else {
                scheduler.scheduleJob(trigger);
            }
        } catch (SchedulerException e) {
            throw new TaskScheduleException("Unable to schedule task #" + task.getId(), e);
        }
    }

    @Override
    public void unschedule(Task task) {
        try {
            JobKey jobKey = taskJobKeyFactory.getStartJobKey(task);

            if (scheduler.checkExists(jobKey)) {
                scheduler.deleteJob(jobKey);
            }
        } catch (SchedulerException e) {
            throw new TaskScheduleException("Unable to unschedule task #" + task.getId(), e);
        }
    }
}

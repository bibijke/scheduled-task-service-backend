package com.koronaauto.service.impl.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.service.task.TaskScheduleService;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.service.task.TaskUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskUpdateServiceImpl implements TaskUpdateService {
    private final TaskService taskService;

    private final TaskScheduleService taskScheduleService;

    @Autowired
    public TaskUpdateServiceImpl(
        TaskService taskService,
        TaskScheduleService taskScheduleService
    ) {
        this.taskService = taskService;
        this.taskScheduleService = taskScheduleService;
    }

    @Override
    public void update(Task source) {
        Task stored = taskService.get(source);

        boolean isScheduleChanged = true;
        boolean isActiveChanged = true;

        if (stored != null) {
            isScheduleChanged = !stored.getSchedule().equals(source.getSchedule());
            isActiveChanged = stored.isActive() != source.isActive();
        }

        stored = taskService.updateOrCreate(source);

        if (isScheduleChanged && stored.isActive()) {
            taskScheduleService.schedule(stored);
        }

        if (isActiveChanged && !stored.isActive()) {
            taskScheduleService.unschedule(stored);
        }
    }
}

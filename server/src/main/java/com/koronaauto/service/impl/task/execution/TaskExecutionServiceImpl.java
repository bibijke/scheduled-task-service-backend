package com.koronaauto.service.impl.task.execution;

import com.koronaauto.entity.impl.task.TaskExecution;
import com.koronaauto.repository.task.TaskExecutionRepository;
import com.koronaauto.service.task.execution.TaskExecutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

@Service
public class TaskExecutionServiceImpl implements TaskExecutionService {
    private final TaskExecutionRepository repo;

    @Autowired
    public TaskExecutionServiceImpl(TaskExecutionRepository repo) {
        this.repo = repo;
    }

    @Override
    public TaskExecution update(TaskExecution taskExecution) {
        return repo.save(taskExecution);
    }

    @Transactional
    @Override
    public void deleteExpired() {
        LocalDateTime date = LocalDateTime
            .now()
            .minus(2, ChronoUnit.DAYS);

        repo.deleteByStartDateBefore(date);
    }
}

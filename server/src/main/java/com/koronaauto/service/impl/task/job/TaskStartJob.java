package com.koronaauto.service.impl.task.job;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.service.task.execution.TaskStartService;
import com.koronaauto.spring.profile.annotation.DevProfile;
import com.koronaauto.spring.profile.annotation.ProdProfile;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

@Component
@ProdProfile
@DevProfile
@DisallowConcurrentExecution
public class TaskStartJob extends QuartzJobBean {
    private final TaskService taskService;

    private final TaskStartService taskStartService;

    @Autowired
    public TaskStartJob(TaskService taskService, TaskStartService taskStartService) {
        this.taskService = taskService;
        this.taskStartService = taskStartService;
    }

    @Override
    protected void executeInternal(JobExecutionContext context) {
        Long taskId = context
            .getJobDetail()
            .getJobDataMap()
            .getLong("task");

        final Task task = taskService.get(taskId);

        taskStartService.start(task);
    }
}

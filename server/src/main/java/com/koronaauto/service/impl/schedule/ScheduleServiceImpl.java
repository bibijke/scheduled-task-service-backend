package com.koronaauto.service.impl.schedule;

import com.koronaauto.service.task.TaskScheduleService;
import com.koronaauto.spring.profile.annotation.DevProfile;
import com.koronaauto.spring.profile.annotation.ProdProfile;
import org.quartz.*;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;
import org.springframework.stereotype.Component;

import org.slf4j.Logger;

@ProdProfile
@DevProfile
@Component
public final class ScheduleServiceImpl implements SmartLifecycle {
    private final Logger logger = LoggerFactory.getLogger(ScheduleServiceImpl.class);

    private boolean isRunning;

    private final Scheduler scheduler;

    private final TaskScheduleService taskScheduleService;

    @Autowired
    public ScheduleServiceImpl(
        Scheduler scheduler,
        TaskScheduleService taskScheduleService
    ) {
        this.scheduler = scheduler;
        this.taskScheduleService = taskScheduleService;
    }

    @Override
    public boolean isAutoStartup() {
        return true;
    }

    @Override
    public void stop(Runnable callback) {
        stop();
        callback.run();
    }

    @Override
    public void start() {
        logger.info("Starting quartz...");

        try {
            scheduler.start();
        } catch (SchedulerException e) {
            throw new RuntimeException("Unable to start quartz", e);
        }

        isRunning = true;

        taskScheduleService.scheduleAll();
    }

    @Override
    public void stop() {
        logger.info("Stopping quartz...");

        try {
            scheduler.shutdown();
        } catch (SchedulerException e) {
            throw new RuntimeException("Unable to stop quartz", e);
        }

        isRunning = false;


    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public int getPhase() {
        return Integer.MAX_VALUE;
    }
}

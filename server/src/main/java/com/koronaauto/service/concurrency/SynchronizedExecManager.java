package com.koronaauto.service.concurrency;

import java.util.function.Supplier;

public interface SynchronizedExecManager<T> {
    void execute(T key, Runnable task);

    <R> R execute(T key, Supplier<R> task);
}

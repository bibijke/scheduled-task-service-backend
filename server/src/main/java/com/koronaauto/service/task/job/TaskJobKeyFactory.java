package com.koronaauto.service.task.job;

import com.koronaauto.entity.impl.task.Task;
import org.quartz.JobKey;

public interface TaskJobKeyFactory {
    JobKey getStartJobKey(Task task);
}

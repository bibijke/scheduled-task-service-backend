package com.koronaauto.service.task;

import com.koronaauto.entity.impl.task.Task;

import java.util.List;

public interface TaskService {
    Task updateOrCreate(Task source);

    Task get(Long id);

    Task get(Task source);

    List<Task> getActiveWithSchedule();

    List<Task> getActive();
}

package com.koronaauto.service.task;

import com.koronaauto.entity.impl.task.Task;

public interface TaskUpdateService {
    void update(Task source);
}

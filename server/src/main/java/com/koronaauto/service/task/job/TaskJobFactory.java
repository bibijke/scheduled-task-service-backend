package com.koronaauto.service.task.job;

import com.koronaauto.entity.impl.task.Task;
import org.quartz.JobDetail;

public interface TaskJobFactory {
    JobDetail getStartJob(Task task);
}

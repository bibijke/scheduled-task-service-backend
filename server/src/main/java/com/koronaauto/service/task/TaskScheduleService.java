package com.koronaauto.service.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.exception.task.TaskScheduleException;

import java.time.LocalDateTime;

public interface TaskScheduleService {
    void scheduleAll()throws TaskScheduleException;

    void schedule(Task task) throws TaskScheduleException;

    void scheduleOnce(Task task, LocalDateTime startAt);

    void unschedule(Task task);
}

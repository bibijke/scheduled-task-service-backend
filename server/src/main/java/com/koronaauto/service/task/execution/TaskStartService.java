package com.koronaauto.service.task.execution;

import com.koronaauto.entity.impl.task.Task;

public interface TaskStartService {
    void start(Task task);
}

package com.koronaauto.service.task.execution;

import com.koronaauto.entity.impl.task.LastTaskExecution;
import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.entity.impl.task.TaskExecution;

public interface LastTaskExecutionService {
    TaskExecution getLastExecution(Task task);

    LastTaskExecution update(Task task, TaskExecution execution);
}

package com.koronaauto.service.task;

import com.koronaauto.entity.impl.task.Task;
import org.quartz.Trigger;

import java.time.LocalDateTime;

public interface TaskTriggerFactory {
    Trigger getCronTrigger(Task task);

    Trigger getSimpleTrigger(Task task, LocalDateTime startDate);
}

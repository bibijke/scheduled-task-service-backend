package com.koronaauto.service.task.execution;

import com.koronaauto.entity.impl.task.TaskExecution;

public interface TaskExecutionService {
    TaskExecution update(TaskExecution taskExecution);

    void deleteExpired();
}

package com.koronaauto.service.task.execution;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.entity.impl.task.TaskExecution;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;

public interface ExternalAppTaskExecutionService {
    TaskExecution refreshProgress(Task task);

    void refreshProgress(Task task, TaskProgress progress);

}

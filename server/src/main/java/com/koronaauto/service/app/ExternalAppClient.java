package com.koronaauto.service.app;

import com.koronaauto.entity.impl.app.ExternalApp;
import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.exception.app.ExternalAppClientException;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;

import java.net.URI;

public interface ExternalAppClient {
    TaskProgress getProgress(Task task) throws ExternalAppClientException;

    StartResponse start(Task task) throws ExternalAppClientException;
}

package com.koronaauto.service.app;

import com.koronaauto.entity.impl.app.ExternalApp;

public interface ExternalAppService {
    ExternalApp get(Long id);
}

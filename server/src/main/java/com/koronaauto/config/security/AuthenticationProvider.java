package com.koronaauto.config.security;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

@Component
public class AuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    protected final String authorization;

    @Autowired
    public AuthenticationProvider(@Value("${security.authorization}") String authorization) {
        this.authorization = authorization;
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails d, UsernamePasswordAuthenticationToken auth) {
        // Nothing to do
    }

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) {

        String token = Optional
            .ofNullable(authentication.getCredentials())
            .map(String::valueOf)
            .orElse(null);

        Collection<GrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority("ROLE_ANONYMOUS"));

        if ((token != null) && token.equals(authorization)) {
            authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        }

        return new User("user", "", authorities);
    }
}
package com.koronaauto.config;

import com.koronaauto.service.concurrency.SynchronizedExecManager;
import com.koronaauto.service.impl.concurrency.SimpleSynchronizedExecManager;
import com.koronaauto.spring.qualifier.task.TaskSynchronizedExecManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TaskConfig {
    @Bean
    @TaskSynchronizedExecManager
    public SynchronizedExecManager<Long> taskSynchronizedExecManager() {
        return new SimpleSynchronizedExecManager<>();
    }
}

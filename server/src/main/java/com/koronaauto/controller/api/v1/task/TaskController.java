package com.koronaauto.controller.api.v1.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.exception.response.ResourceBadRequest;
import com.koronaauto.exception.response.ResourceInternalException;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.service.task.TaskUpdateService;
import com.koronaauto.spring.security.annotation.AuthorizationRequired;
import com.koronaauto.transport.dto.task.TaskGetResponse;
import com.koronaauto.transport.dto.task.TaskUpdateRequest;
import com.koronaauto.transport.mapping.task.TaskGetMapper;
import com.koronaauto.transport.mapping.task.TaskUpdateMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Validated
@RestController
@RequestMapping(value = "/api/v1/task")
@Api(tags = "Task")
public class TaskController {
    private final TaskUpdateMapper updateMapper;

    private final TaskUpdateService taskUpdateService;

    private final TaskGetMapper getMapper;

    private final TaskService taskService;

    @Autowired
    public TaskController(
        TaskUpdateMapper updateMapper,
        TaskUpdateService taskUpdateService,
        TaskGetMapper getMapper,
        TaskService taskService
    ) {
        this.updateMapper = updateMapper;
        this.taskUpdateService = taskUpdateService;
        this.getMapper = getMapper;
        this.taskService = taskService;
    }

    @AuthorizationRequired
    @PostMapping(value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Update task settings")
    public void update(@Valid @RequestBody TaskUpdateRequest request) {
        Task task;

        try {
            task = updateMapper.toDomain(request);

            taskUpdateService.update(task);
        } catch (IllegalArgumentException e) {
            throw new ResourceBadRequest(e.getMessage());
        } catch (Exception e) {
            throw new ResourceInternalException(e.getMessage());
        }
    }

    @AuthorizationRequired
    @GetMapping(value = "/list")
    @ApiOperation(value = "Get tasks list")
    public List<TaskGetResponse> list() {
        List<TaskGetResponse> responses;

        try {
            responses = taskService
                .getActive()
                .stream()
                .map(getMapper::toDomain)
                .collect(Collectors.toList());
        } catch (IllegalArgumentException e) {
            throw new ResourceBadRequest(e.getMessage());
        } catch (Exception e) {
            throw new ResourceInternalException(e.getMessage());
        }

        return responses;
    }
}

package com.koronaauto.controller.api.v1.task;

import com.koronaauto.exception.response.ResourceBadRequest;
import com.koronaauto.scheduledtaskservice.controller.impl.SimpleScheduledTaskController;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTaskService;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;
import com.koronaauto.spring.security.annotation.AuthorizationRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/background-task", consumes = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Task: Background tasks")
public class BackgroundTaskController extends SimpleScheduledTaskController {
    @Autowired
    public BackgroundTaskController(ManagedTaskService taskService) {
        super(taskService);
    }

    @AuthorizationRequired
    @PostMapping("/start")
    @ApiOperation(value = "Start tasks")
    @Override
    public List<StartResponse> start(@RequestParam List<Long> tasks) {
        return super.start(tasks);
    }

    @AuthorizationRequired
    @GetMapping("/progress")
    @ApiOperation(value = "Get tasks progress")
    @Override
    public List<TaskProgress> getProgress(@RequestParam List<Long> tasks) {
        try {
            return super.getProgress(tasks);
        } catch (IllegalArgumentException e) {
            throw new ResourceBadRequest(e.getMessage());
        }
    }
}

package com.koronaauto.controller.api.v1.task;


import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.exception.response.ResourceBadRequest;
import com.koronaauto.exception.response.ResourceInternalException;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.service.task.TaskScheduleService;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.service.task.execution.ExternalAppTaskExecutionService;
import com.koronaauto.spring.security.annotation.AuthorizationRequired;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Optional;

@Validated
@RestController
@RequestMapping(value = "/api/v1/task/execution", consumes = MediaType.APPLICATION_JSON_VALUE)
@Api(tags = "Task: Execution")
public class TaskExecutionController {
    private final TaskService taskService;

    private final ExternalAppTaskExecutionService externalAppTaskExecutionService;

    private final TaskScheduleService taskScheduleService;

    @Autowired
    public TaskExecutionController(
        TaskService taskService,
        ExternalAppTaskExecutionService externalAppTaskExecutionService,
        TaskScheduleService taskScheduleService
    ) {
        this.taskService = taskService;
        this.externalAppTaskExecutionService = externalAppTaskExecutionService;
        this.taskScheduleService = taskScheduleService;
    }

    @AuthorizationRequired
    @PostMapping("/progress")
    @ApiOperation(value = "Update task execution progress")
    public void update(
        @RequestParam Long taskId,
        @Valid @RequestBody TaskProgress progress
    ) {
        Task task = Optional
            .ofNullable(taskService.get(taskId))
            .orElseThrow(() -> new ResourceBadRequest("Task #"+taskId+" does not exist"));

        try {
            externalAppTaskExecutionService.refreshProgress(task, progress);
        } catch (Exception e) {
            throw new ResourceBadRequest(e.getMessage());
        }
    }

    @AuthorizationRequired
    @PostMapping("")
    @ApiOperation(value = "Execute task")
    public void create(@RequestParam Long taskId) {
        Task task = Optional
            .ofNullable(taskService.get(taskId))
            .orElseThrow(() -> new ResourceBadRequest("Task #"+taskId+" does not exist"));

        try {
            taskScheduleService.scheduleOnce(task, LocalDateTime.now());
        } catch (Exception e) {
            throw new ResourceInternalException(e.getMessage());
        }
    }
}

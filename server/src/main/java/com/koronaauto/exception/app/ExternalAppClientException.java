package com.koronaauto.exception.app;

public class ExternalAppClientException extends RuntimeException {
    public ExternalAppClientException(String message, Throwable cause) {
        super(message, cause);
    }
}

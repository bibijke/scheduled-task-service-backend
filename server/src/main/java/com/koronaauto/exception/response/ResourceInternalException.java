package com.koronaauto.exception.response;

public class ResourceInternalException extends RuntimeException {
    public ResourceInternalException(String message) {
        super(message);
    }
}

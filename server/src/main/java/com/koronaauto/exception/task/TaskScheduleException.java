package com.koronaauto.exception.task;

public class TaskScheduleException extends RuntimeException {
    public TaskScheduleException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskScheduleException(String message) {
        super(message);
    }
}

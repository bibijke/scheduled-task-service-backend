package com.koronaauto.entity;

public interface IdentifiableEntity {
    Long getId();

    void setId(Long id);
}

package com.koronaauto.entity.impl.app;

import com.koronaauto.entity.impl.SimpleIdentifiableEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "external_app")
public class ExternalApp extends SimpleIdentifiableEntity {
    private String name;

    @Column(name = "base_url")
    private String baseUrl;

    @Column(name = "access_token")
    private String accessToken;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}

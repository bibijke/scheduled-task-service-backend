package com.koronaauto.entity.impl.task;

public enum TaskExecutionState {
    START_ERROR,
    RUNNING,
    COMPLETED,
    FAILED
}

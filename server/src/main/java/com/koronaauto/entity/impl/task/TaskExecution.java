package com.koronaauto.entity.impl.task;

import com.koronaauto.entity.impl.SimpleIdentifiableEntity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "task_execution")
public class TaskExecution extends SimpleIdentifiableEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "task_id")
    private Task task;

    @Enumerated
    private TaskExecutionState state;

    @Column(name = "start_date")
    private LocalDateTime startDate;

    @Column(name = "end_date")
    private LocalDateTime endDate;

    private String description;

    private float rate;

    private long duration;

    private long remaining;

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public TaskExecutionState getState() {
        return state;
    }

    public void setState(TaskExecutionState state) {
        this.state = state;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public long getRemaining() {
        return remaining;
    }

    public void setRemaining(long remaining) {
        this.remaining = remaining;
    }

    public boolean isFinished() {
        TaskExecutionState s = getState();

        return !s.equals(TaskExecutionState.RUNNING);
    }
}

package com.koronaauto.entity.impl.task;

import com.koronaauto.entity.impl.SimpleIdentifiableEntity;
import com.koronaauto.entity.impl.app.ExternalApp;
import org.quartz.CronExpression;

import javax.persistence.*;
import java.text.ParseException;
import java.time.LocalDateTime;

@Entity
@Table(name = "task")
public class Task extends SimpleIdentifiableEntity {
    private String schedule;

    private String name;

    @ManyToOne
    @JoinColumn(name = "app_id")
    private ExternalApp app;

    @Column(name = "update_date")
    private LocalDateTime updateDate = LocalDateTime.now();

    private boolean active;

    public String getSchedule() {
        return schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSchedule(String schedule) throws IllegalArgumentException {
        try {
            new CronExpression(schedule);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e.getMessage());
        }
        this.schedule = schedule;
    }

    public ExternalApp getApp() {
        return app;
    }

    public void setApp(ExternalApp app) {
        this.app = app;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

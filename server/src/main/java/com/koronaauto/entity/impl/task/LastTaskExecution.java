package com.koronaauto.entity.impl.task;

import javax.persistence.*;
import java.util.Optional;

@Entity
@Table(name = "task_execution_last")
public class LastTaskExecution {
    @Id
    @Column(name = "task_id")
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "task_id")
    @PrimaryKeyJoinColumn(name = "task_id", referencedColumnName = "id")
    private Task task;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "execution_id")
    private TaskExecution execution;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;

        Long id = Optional
            .ofNullable(task)
            .map(Task::getId)
            .orElse(null);

        setId(id);
    }

    public TaskExecution getExecution() {
        return execution;
    }

    public void setExecution(TaskExecution execution) {
        this.execution = execution;
    }
}

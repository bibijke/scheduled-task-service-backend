package com.koronaauto.spring.controller.exception;

import com.koronaauto.exception.response.ResourceBadRequest;
import com.koronaauto.exception.response.ResourceInternalException;
import com.koronaauto.transport.dto.response.ResourceError;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestResponseEntityExceptionHandler {
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ResourceInternalException.class)
    protected ResourceError handleInternalError(ResourceInternalException e) {
        return new ResourceError("INTERNAL_SERVER_ERROR", e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ResourceBadRequest.class)
    protected ResourceError handleBadRequest(ResourceBadRequest e) {
        return new ResourceError("BAD_REQUEST", e.getMessage());
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResourceError handleBadRequest(MethodArgumentNotValidException e) {
        return new ResourceError("BAD_REQUEST", e.getMessage());
    }

    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ExceptionHandler(AccessDeniedException.class)
    protected ResourceError handleBadRequest(AccessDeniedException e) {
        return new ResourceError("ACCESS_DENIED", e.getMessage());
    }
}

package com.koronaauto.spring.profile.annotation;

import com.koronaauto.spring.profile.ProfileType;
import org.springframework.context.annotation.Profile;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Profile({ProfileType.DEV})
public @interface DevProfile {}

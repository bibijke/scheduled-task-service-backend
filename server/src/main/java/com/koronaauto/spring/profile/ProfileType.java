package com.koronaauto.spring.profile;

public final class ProfileType {
    public final static String PROD = "prod";
    public final static String DEV = "dev";
    public final static String TEST = "test";
}

package com.koronaauto.transport.mapping.task;

import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.transport.dto.task.TaskGetResponse;
import com.koronaauto.transport.mapping.app.ExternalAppGetMapper;
import org.mapstruct.Mapper;

@Mapper(uses = {ExternalAppGetMapper.class})
public abstract class TaskGetMapper {
    public abstract TaskGetResponse toDomain(Task t);
}

package com.koronaauto.transport.mapping.task;

import com.koronaauto.entity.impl.task.Task;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper
public abstract class TaskMapper {
    @Mapping(target = "id", ignore = true)
    public abstract void copyToDomain(Task source, @MappingTarget Task target);
}

package com.koronaauto.transport.mapping.app;

import com.koronaauto.entity.impl.app.ExternalApp;
import com.koronaauto.transport.dto.app.ExternalAppGetResponse;
import org.mapstruct.Mapper;

@Mapper
public abstract class ExternalAppGetMapper {
    public abstract ExternalAppGetResponse toDomain(ExternalApp app) ;
}

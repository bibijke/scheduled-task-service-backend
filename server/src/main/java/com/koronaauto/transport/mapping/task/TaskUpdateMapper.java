package com.koronaauto.transport.mapping.task;

import com.koronaauto.entity.impl.app.ExternalApp;
import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.service.app.ExternalAppService;
import com.koronaauto.transport.dto.task.TaskUpdateRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

@Mapper
public abstract class TaskUpdateMapper {
    @Autowired
    private ExternalAppService appService;

    @Mapping(target = "app", ignore = true)
    abstract Task toDomainInternal(TaskUpdateRequest r);

    public Task toDomain(TaskUpdateRequest r) throws IllegalArgumentException {
        Task task = toDomainInternal(r);

        ExternalApp app = Optional
            .ofNullable(appService.get(r.getAppId()))
            .orElseThrow(() -> new IllegalArgumentException("App #"+r.getAppId()+" does not exists"));

        task.setApp(app);

        return task;
    }
}

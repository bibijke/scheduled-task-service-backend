package com.koronaauto.transport.dto.task;

import com.koronaauto.transport.dto.app.ExternalAppGetResponse;

import java.time.LocalDateTime;

public class TaskGetResponse {
    private Long id;

    private String schedule;

    private String name;

    private ExternalAppGetResponse app;

    private LocalDateTime updateDate;

    private boolean active;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ExternalAppGetResponse getApp() {
        return app;
    }

    public void setApp(ExternalAppGetResponse app) {
        this.app = app;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

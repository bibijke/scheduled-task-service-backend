package com.koronaauto.repository.app;

import com.koronaauto.entity.impl.app.ExternalApp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExternalAppRepository extends JpaRepository<ExternalApp, Long> {
}

package com.koronaauto.repository.task;

import com.koronaauto.entity.impl.task.TaskExecution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;

@Repository
public interface TaskExecutionRepository extends JpaRepository<TaskExecution, Long> {
    void deleteByStartDateBefore(LocalDateTime date);
}
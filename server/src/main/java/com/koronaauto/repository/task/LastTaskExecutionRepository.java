package com.koronaauto.repository.task;

import com.koronaauto.entity.impl.task.LastTaskExecution;
import com.koronaauto.entity.impl.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LastTaskExecutionRepository extends JpaRepository<LastTaskExecution, Long> {
    LastTaskExecution findByTask(Task t);
}

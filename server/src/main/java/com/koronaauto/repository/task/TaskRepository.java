package com.koronaauto.repository.task;

import com.koronaauto.entity.impl.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByActiveTrueAndScheduleNotNull();

    List<Task> findByActiveTrue();
}

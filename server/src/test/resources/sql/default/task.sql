INSERT INTO public.task (id, schedule, name, update_date, app_id, active)
VALUES (1, '0/5 * * * * ?', 'Test Task #1', '2020-06-08 19:00:43.968569', 1, true);

INSERT INTO public.task (id, schedule, name, update_date, app_id, active)
VALUES (2, '0/20 * * * * ?', 'Test Task #2', '2020-06-08 16:30:29.000000', 1, true);

INSERT INTO public.task (id, schedule, name, update_date, app_id, active)
VALUES (3, '0/5 * * * * ?', 'Test Task #3', '2020-06-08 19:00:43.968569', 2, true);
INSERT INTO public.task_execution (id, task_id, state, start_date, end_date, description)
 VALUES (3, 3, 1, now(), null, null);

INSERT INTO public.task_execution_last (task_id, execution_id)
 VALUES (3, 3);

INSERT INTO public.task (id, schedule, name, update_date, app_id, active)
VALUES (4, '0/5 * * * * ?', 'Test Task #4', '2020-06-08 19:00:43.968569', 2, true);

INSERT INTO public.task_execution (id, task_id, state, start_date, end_date, description)
VALUES (4, 4, 1, now(), null, null);

INSERT INTO public.task_execution_last (task_id, execution_id)
VALUES (4, 4);
package com.koronaauto.service.impl.concurrency;


import com.koronaauto.service.concurrency.SynchronizedExecManager;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SynchronizedExecManagerTest {
    @Test
    public void testExecute() throws Exception {
        final int poolSize = 5;

        SimpleSynchronizedExecManager<Long> taskManager = new SimpleSynchronizedExecManager<>();

        ExecutorService executorService = Executors.newFixedThreadPool(poolSize);

        List<Map<Integer, String>> threadLogs = new ArrayList<>();

        for (int i = 0; i < poolSize; i++) {
            final int index = i;

            Runnable threadTask = () -> {
                boolean res = taskManager.execute(1L, () -> {
                    threadLogs.add(Collections.singletonMap(index, "start"));

                    try {
                        Thread.sleep(10 - index*2);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    threadLogs.add(Collections.singletonMap(index, "done"));

                    return true;
                });

                assertTrue(res);
            };

            executorService.execute(threadTask);
        }

        boolean executed = !executorService.awaitTermination(1, TimeUnit.SECONDS);

        assertTrue(executed);

        executorService.shutdownNow();

        assertEquals(poolSize * 2, threadLogs.size());

        for (int i = 0; i < poolSize * 2; i += 2) {
            final Map<Integer, String> startLog = threadLogs.get(i);
            final Map<Integer, String> doneLog = threadLogs.get(i+1);

            Map.Entry<Integer, String> startLogEntry = startLog.entrySet().iterator().next();
            Map.Entry<Integer, String> doneLogEntry = doneLog.entrySet().iterator().next();

            assertEquals("start", startLogEntry.getValue());
            assertEquals("done", doneLogEntry.getValue());

            assertEquals(startLogEntry.getKey(), doneLogEntry.getKey());
        }

        checkTaskManagerMapsCleaned(taskManager);
    }

    @SuppressWarnings("unchecked")
    private <T> void checkTaskManagerMapsCleaned(SimpleSynchronizedExecManager<T> taskManager) throws Exception {
        Field field = SimpleSynchronizedExecManager.class.getDeclaredField("lockMap");

        field.setAccessible(true);

        ConcurrentMap<T, Lock> lockMap = (ConcurrentMap<T, Lock>) field.get(taskManager);

        assertEquals(0, lockMap.size());

        field = SimpleSynchronizedExecManager.class.getDeclaredField("lockCountMap");

        field.setAccessible(true);

        Map<T, Integer> lockCountMap = (Map<T, Integer>) field.get(taskManager);

        assertEquals(0, lockCountMap.size());
    }
}

package com.koronaauto.service.impl.task.execution;

import com.koronaauto.SpringBootTest;
import com.koronaauto.database.annotation.RestoreTestDatabase;
import com.koronaauto.entity.impl.task.Task;
import com.koronaauto.entity.impl.task.TaskExecution;
import com.koronaauto.entity.impl.task.TaskExecutionState;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskState;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;
import com.koronaauto.service.app.ExternalAppClient;
import com.koronaauto.service.task.TaskService;
import com.koronaauto.service.task.execution.LastTaskExecutionService;
import com.koronaauto.service.task.execution.TaskStartService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import javax.transaction.Transactional;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@SqlGroup({
    @Sql("/sql/service/impl/task/execution/task_start_service_test.sql"),
})
@Order
@interface RestoreTestCase {
}

@Transactional
@RestoreTestDatabase
@RestoreTestCase
public class TaskStartServiceTest extends SpringBootTest {
    @Autowired
    private TaskService taskService;

    @Autowired
    private TaskStartService taskStartService;

    @Autowired
    private ExternalAppClient externalAppClient;

    @Autowired
    private LastTaskExecutionService lastTaskExecutionService;

    @Test
    public void testStart() {
        testSuccessfulStart();

        testFailedStart();

        testStartWhenRunning();

        testStartWhenStateChanged();
    }

    private void testSuccessfulStart() {
        Task task = taskService.get(1L);

        StartResponse response = new StartResponse();

        response.setStarted(true);

        Mockito
            .when(externalAppClient.start(any()))
            .thenReturn(response);

        taskStartService.start(task);

        TaskExecution execution = lastTaskExecutionService.getLastExecution(task);

        assertNotNull(execution);

        assertEquals(TaskExecutionState.RUNNING, execution.getState());
    }

    private void testFailedStart() {
        Task task = taskService.get(2L);

        StartResponse response = new StartResponse();

        response.setStarted(false);

        Mockito
            .when(externalAppClient.start(any()))
            .thenReturn(response);

        taskStartService.start(task);

        TaskExecution execution = lastTaskExecutionService.getLastExecution(task);

        assertNotNull(execution);

        assertEquals(TaskExecutionState.START_ERROR, execution.getState());
    }

    private void testStartWhenRunning() {
        Task task = taskService.get(3L);

        StartResponse response = new StartResponse();

        response.setStarted(false);

        Mockito
            .when(externalAppClient.start(any()))
            .thenReturn(response);

        TaskProgress progress = new TaskProgress();

        progress.setState(TaskState.RUNNING);
        progress.setRate(0.5f);

        Mockito
            .when(externalAppClient.getProgress(any()))
            .thenReturn(progress);

        taskStartService.start(task);

        TaskExecution execution = lastTaskExecutionService.getLastExecution(task);

        assertNotNull(execution);

        assertEquals(TaskExecutionState.START_ERROR, execution.getState());

        assertEquals("Last task execution has not been finished yet", execution.getDescription());
    }

    private void testStartWhenStateChanged() {
        Task task = taskService.get(4L);

        StartResponse response = new StartResponse();

        response.setStarted(true);

        Mockito
            .when(externalAppClient.start(any()))
            .thenReturn(response);

        TaskProgress progress = new TaskProgress();

        progress.setState(TaskState.COMPLETED);
        progress.setRate(1.0f);

        Mockito
            .when(externalAppClient.getProgress(any()))
            .thenReturn(progress);

        taskStartService.start(task);

        TaskExecution execution = lastTaskExecutionService.getLastExecution(task);

        assertNotNull(execution);

        assertEquals(TaskExecutionState.RUNNING, execution.getState());

        assertNull(execution.getDescription());
    }
}

package com.koronaauto;

import com.koronaauto.service.app.ExternalAppClient;
import com.koronaauto.service.task.TaskScheduleService;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.MockBeans;

@org.springframework.boot.test.context.SpringBootTest()
@MockBeans({
    @MockBean(ExternalAppClient.class),
    @MockBean(TaskScheduleService.class)
})
public abstract class SpringBootTest {}

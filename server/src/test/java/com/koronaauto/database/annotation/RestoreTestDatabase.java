package com.koronaauto.database.annotation;

import org.springframework.core.annotation.Order;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@SqlGroup({
    @Sql("/sql/default/external_app.sql"),
    @Sql("/sql/default/task.sql")
})
@Order(0)
public @interface RestoreTestDatabase {}

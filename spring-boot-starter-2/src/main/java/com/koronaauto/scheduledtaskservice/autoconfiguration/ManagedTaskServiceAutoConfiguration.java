package com.koronaauto.scheduledtaskservice.autoconfiguration;

import com.koronaauto.scheduledtaskservice.service.impl.task.SynchronizedManagedTaskService;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTask;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass({ManagedTaskService.class})
public class ManagedTaskServiceAutoConfiguration {
    private final ManagedTask[] tasks;

    @Autowired
    public ManagedTaskServiceAutoConfiguration(ManagedTask[] tasks) {
        this.tasks = tasks;
    }

    @Bean
    @ConditionalOnMissingBean
    public ManagedTaskService managedTaskService() {
        ManagedTaskService mts = new SynchronizedManagedTaskService();

        for (ManagedTask task : tasks) {
            mts.register(task);
        }

        return mts;
    }
}

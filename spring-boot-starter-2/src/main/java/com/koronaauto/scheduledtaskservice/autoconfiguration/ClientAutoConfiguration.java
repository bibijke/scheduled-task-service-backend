package com.koronaauto.scheduledtaskservice.autoconfiguration;

import com.koronaauto.scheduledtaskservice.service.Client;
import com.koronaauto.scheduledtaskservice.service.impl.HttpClient;
import com.koronaauto.scheduledtaskservice.service.impl.task.NonConcurrentTask;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(NonConcurrentTask.class)
@EnableConfigurationProperties(ClientProperties.class)
public class ClientAutoConfiguration {
    @Bean
    @ConditionalOnMissingBean
    public Client client(ClientProperties properties) {
        return new HttpClient(properties.getHost(), properties.getAuthorization());
    }
}

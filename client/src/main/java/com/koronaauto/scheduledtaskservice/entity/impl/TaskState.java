package com.koronaauto.scheduledtaskservice.entity.impl;

public enum TaskState {
    IDLE,
    STOPPING,
    STOPPED,
    RUNNING,
    COMPLETED,
    FAILED
}

package com.koronaauto.scheduledtaskservice.entity.impl;


import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;

public final class TaskProgress implements Cloneable, Serializable {

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private float rate;

    private long durationMs;

    private long remainingTimeMs;

    private TaskState state;

    private String errorMessage;

    public TaskProgress() {
        clear();
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public float getRate() {
        return rate;
    }

    public long getDurationMs() {
        return durationMs;
    }

    public void setDurationMs(long durationMs) {
        this.durationMs = durationMs;
    }

    public long getRemainingTimeMs() {
        return remainingTimeMs;
    }

    public void setRemainingTimeMs(long remainingTimeMs) {
        this.remainingTimeMs = remainingTimeMs;
    }

    public TaskState getState() {
        return state;
    }

    public void setState(TaskState state) {
        this.state = state;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public void complete() {
        if (getState() == TaskState.STOPPING) {
            setState(TaskState.STOPPED);
        } else {
            setRate(1);
            setState(TaskState.COMPLETED);
        }

        finish();
    }

    public void fail(String errorMessage) {
        setState(TaskState.FAILED);

        setErrorMessage(errorMessage);

        finish();
    }

    private void finish() {
        setEndDate(LocalDateTime.now());
        setDurationMs(Duration.between(getStartDate(), getEndDate()).toMillis());
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    private void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    private void clear() {
        this.rate = 0;
        setState(TaskState.COMPLETED);
        setStartDate(null);
        setEndDate(null);
        setDurationMs(0);
        setRemainingTimeMs(0);
    }

    public void start() {
        clear();

        setState(TaskState.RUNNING);
        setStartDate(LocalDateTime.now());
    }

    @Override
    public TaskProgress clone()  {
        TaskProgress tp;

        try {
            tp = (TaskProgress) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("Clone is not available for this object");
        }

        tp.setState(this.getState());
        tp.setStartDate(this.getStartDate());
        tp.setEndDate(this.getEndDate());
        tp.setRate(this.getRate());
        tp.setErrorMessage(this.getErrorMessage());

        return tp;
    }
}

package com.koronaauto.scheduledtaskservice.controller;

import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;

import java.util.List;

public interface ScheduledTaskController {
    List<StartResponse> start(List<Long> tasks);

    List<TaskProgress> getProgress(List<Long> tasks);
}

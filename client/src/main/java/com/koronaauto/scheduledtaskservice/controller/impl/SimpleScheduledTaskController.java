package com.koronaauto.scheduledtaskservice.controller.impl;

import com.koronaauto.scheduledtaskservice.controller.ScheduledTaskController;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTask;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTaskService;
import com.koronaauto.scheduledtaskservice.transport.dto.task.StartResponse;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SimpleScheduledTaskController implements ScheduledTaskController {
    private final ManagedTaskService taskService;

    public SimpleScheduledTaskController(ManagedTaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public List<StartResponse> start(List<Long> tasks) {
        return tasks
            .stream()
            .map(id -> {
                boolean started = Optional
                    .ofNullable(taskService.get(id))
                    .map(ManagedTask::start)
                    .orElse(false);

                StartResponse response = new StartResponse();

                response.setStarted(started);

                return response;
            })
            .collect(Collectors.toList());
    }

    @Override
    public List<TaskProgress> getProgress(List<Long> tasks) {
        return tasks
            .stream()
            .map(id -> Optional
                .ofNullable(taskService.get(id))
                .map(ManagedTask::getProgress)
                .orElseThrow(() -> new IllegalArgumentException("Task #"+id+" does not exist"))
            )
            .collect(Collectors.toList());
    }
}

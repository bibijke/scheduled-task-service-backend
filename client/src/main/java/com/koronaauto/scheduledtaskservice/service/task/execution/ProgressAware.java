package com.koronaauto.scheduledtaskservice.service.task.execution;

public interface ProgressAware {
    void setProgress(float rate);
}

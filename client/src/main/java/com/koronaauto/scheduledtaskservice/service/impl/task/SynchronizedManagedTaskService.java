package com.koronaauto.scheduledtaskservice.service.impl.task;

import com.koronaauto.scheduledtaskservice.service.task.ManagedTask;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTaskService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class SynchronizedManagedTaskService implements ManagedTaskService {
    private final Map<Long, ManagedTask> taskMap = new ConcurrentHashMap<>();

    @Override
    public void register(ManagedTask task) {
        taskMap.put(task.getId(), task);
    }

    @Override
    public ManagedTask get(Long id) {
        return taskMap.get(id);
    }
}

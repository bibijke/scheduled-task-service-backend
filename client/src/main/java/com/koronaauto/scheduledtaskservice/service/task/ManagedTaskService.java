package com.koronaauto.scheduledtaskservice.service.task;

public interface ManagedTaskService {
   void register(ManagedTask task);

   ManagedTask get(Long id);
}

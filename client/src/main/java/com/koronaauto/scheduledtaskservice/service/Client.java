package com.koronaauto.scheduledtaskservice.service;

import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.exception.client.ClientException;

public interface Client {
    void sendProgress(Long taskId, TaskProgress progress) throws ClientException;
}

package com.koronaauto.scheduledtaskservice.service.task;

import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;

public interface ManagedTask {
    Long getId();

    boolean start();

    boolean stop();

    TaskProgress getProgress();
}

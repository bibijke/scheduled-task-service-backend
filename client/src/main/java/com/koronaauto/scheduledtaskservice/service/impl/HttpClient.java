package com.koronaauto.scheduledtaskservice.service.impl;

import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.exception.client.ClientException;
import com.koronaauto.scheduledtaskservice.service.Client;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriBuilder;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Collections;
import java.util.Map;

public class HttpClient implements Client {
    private final String baseUrl;

    private final String authorization;

    private final RestTemplate template;

    public HttpClient(String baseUrl, String authorization) {
        this.baseUrl = baseUrl;
        this.authorization = authorization;

        HttpComponentsClientHttpRequestFactory httpRequestFactory = new HttpComponentsClientHttpRequestFactory();

        httpRequestFactory.setConnectTimeout(3000);
        httpRequestFactory.setReadTimeout(3000);

        template = new RestTemplate(httpRequestFactory);
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    private <T> HttpEntity<T> getRequest(T content) {
        HttpHeaders headers = new HttpHeaders();

        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set(HttpHeaders.AUTHORIZATION, authorization);

        return new HttpEntity<>(content, headers);
    }

    @Override
    public void sendProgress(Long taskId, TaskProgress progress) throws ClientException {
        UriBuilder uriBuilder = UriComponentsBuilder
            .fromUriString(getBaseUrl()+"/api/v1/task/execution/progress");

        uriBuilder.queryParam("taskId", taskId);

        try {
            template.exchange(
                uriBuilder.build(),
                HttpMethod.POST,
                getRequest(progress),
                Void.class
            );
        } catch (RestClientException e) {
            throw new ClientException("Unable to send progress", e);
        }
    }
}

package com.koronaauto.scheduledtaskservice.service.impl.task;


import com.koronaauto.scheduledtaskservice.entity.impl.TaskProgress;
import com.koronaauto.scheduledtaskservice.entity.impl.TaskState;
import com.koronaauto.scheduledtaskservice.exception.client.ClientException;
import com.koronaauto.scheduledtaskservice.service.Client;
import com.koronaauto.scheduledtaskservice.service.task.ManagedTask;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class NonConcurrentTask implements ManagedTask {
    private final Long id;

    private final Client client;

    private final AtomicBoolean operationIsRunning = new AtomicBoolean(false);

    private final TaskProgress progress = new TaskProgress();

    protected NonConcurrentTask(Long id, Client client) {
        this.id = id;
        this.client = client;
    }

    @Override
    public Long getId() {
        return id;
    }

    protected void setProgress(float rate) {
        if ((rate < 0) || (rate > 1)) {
            throw new IllegalArgumentException("Rate must be in range [0, 1]");
        }

        synchronized (progress) {
            progress.setRate(rate);

            if (progress.getRate() == 0.0) {
                progress.setDurationMs(0);
                progress.setRemainingTimeMs(0);

            } else {
                progress.setDurationMs(Duration.between(progress.getStartDate(), LocalDateTime.now()).toMillis());
                progress.setRemainingTimeMs(Math.round(progress.getDurationMs() / rate) - progress.getDurationMs());
            }
        }
    }

    @Override
    public boolean start() {
        if (Thread.interrupted()) {
            return false;
        }

        if (operationIsRunning.compareAndSet(false, true)) {
            progress.start();

            Thread thread = createMainThread();

            thread.start();
        } else {
            return false;
        }

        return true;
    }

    private Thread createMainThread() {
        return new Thread(() -> {
            final ScheduledExecutorService progressExecutor = Executors.newSingleThreadScheduledExecutor();

            Exception exception = null;

            try {
                onStart();

                progressExecutor.scheduleAtFixedRate(this::sendProgress, 0, 10, TimeUnit.SECONDS);

                perform();

            } catch (Exception e) {
                exception = e;
            }

            progressExecutor.shutdown();

            try {
                progressExecutor.awaitTermination(3, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                onSendProgressException(e);
            }

            if (exception == null) {
                progress.complete();

                onComplete();
            } else {
                progress.fail(exception.getMessage());

                onFailure(exception);
            }

            sendProgress();

            operationIsRunning.set(false);
        });
    }

    @Override
    public boolean stop() {
        boolean result = false;

        synchronized (progress) {
            if (progress.getState() == TaskState.RUNNING) {
                progress.setState(TaskState.STOPPING);

                result = true;
            }
        }

        return result;
    }

    private void sendProgress() {
        if (!Thread.interrupted() && operationIsRunning.get()) {
            TaskProgress progress = getProgress();

            try {
                client.sendProgress(getId(), progress);
            } catch (ClientException e) {
                onSendProgressException(e);
            }
        }
    }

    @Override
    public TaskProgress getProgress() {
        TaskProgress clone;

        synchronized (progress) {
            clone = progress.clone();
        }

        return clone;
    }

    protected abstract void perform();

    protected void onStart() { }

    protected void onComplete() { }

    protected void onFailure(Exception e) { }

    protected void onSendProgressException(Exception e) {}
}

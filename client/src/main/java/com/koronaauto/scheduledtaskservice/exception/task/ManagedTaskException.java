package com.koronaauto.scheduledtaskservice.exception.task;

public class ManagedTaskException extends Exception {

    public ManagedTaskException(String message) {
        super(message);
    }
}

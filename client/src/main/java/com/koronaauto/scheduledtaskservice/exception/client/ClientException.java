package com.koronaauto.scheduledtaskservice.exception.client;

public class ClientException extends Exception {
    public ClientException(String message, Throwable cause) {
        super(message, cause);
    }
}

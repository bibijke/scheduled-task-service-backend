package com.koronaauto.scheduledtaskservice.exception.task;

public class OperationIsAlreadyRunningException extends ManagedTaskException {

    private final String reasonCode;

    public OperationIsAlreadyRunningException() {

        super("Operation is already running.");

        this.reasonCode = "OPERATION_IS_ALREADY_RUNNING";
    }

    public String getReasonCode() {
        return reasonCode;
    }
}

package com.koronaauto.scheduledtaskservice.transport.dto.task;

public class StartResponse {
    private boolean started;

    public boolean isStarted() {
        return started;
    }

    public void setStarted(boolean started) {
        this.started = started;
    }
}

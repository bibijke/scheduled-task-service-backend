# README #

Бекенд сервиса задач по расписанию.

Цель микросервиса, администртирование/запуск/контроль/логирование задач, которые необходимо запускать по расписанию/разово
 в других микросервисах.

Построен на базе quartz (quartz-scheduler.org).
Имеет spring starter, библиотеку с клиентом, а так же сам сервис.

Для запуска необходим gradle (тестировалось на версии 6), java 8.

Для запуска использовать команду gradle bootRun.

P.S. Перед запуском необходимо настроить конфигурационные файлы:

`server/src/main/resources/application-business-config-defaults.yml`

`server/src/test/resources/application-business-config-defaults.yml`

которые можно развернуть из шаблонов с префиксом .dist в тех же папках.

Если что-то не получилось, пожалуйста, свяжитесь со мной i@dmitriy-harin.ru.
